#!/bin/sh

version=$(git log -n1 --date=format:'%y%m%d' | grep Date: | grep -o '[[:digit:]]*')'-'$(git rev-parse --short=8 HEAD)
echo $version
